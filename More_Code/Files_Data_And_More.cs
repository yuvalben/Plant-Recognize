﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Files_Data_And_More
{
    class Program
    {
        // Helper Functions
        static int CountFilesInFolder(string FolderPath, string ex)
        {
            return (from file in Directory.EnumerateFiles(@FolderPath, "*." + ex, SearchOption.AllDirectories)
                    select file).Count();
        }

        /// <summary>
        ///  Copy File From Source to Target Path
        /// </summary>
        /// <param name="sourcePath"> Source Path</param>
        /// <param name="targetPath"> Target Path</param>
        static void Copy(string sourcePath, string targetPath)
        {

            // Get our files (recursive and any of them, based on the 2nd param of the Directory.GetFiles() method
            string[] originalFiles = Directory.GetFiles(sourcePath, "*", SearchOption.AllDirectories);

            // Dealing with a string array, so let's use the actionable Array.ForEach() with a anonymous method
            Array.ForEach(originalFiles, (originalFileLocation) =>
            {
                // Get the FileInfo for both of our files
                FileInfo originalFile = new FileInfo(originalFileLocation);
                FileInfo destFile = new FileInfo(originalFileLocation.Replace(sourcePath, targetPath));
                // ^^ We can fill the FileInfo() constructor with files that don't exist...

                // ... because we check it here
                if (destFile.Exists)
                {
                    // Logic for files that exist applied here; if the original is larger, replace the updated files...
                    if (originalFile.Length > destFile.Length)
                    {
                        originalFile.CopyTo(destFile.FullName, true);
                    }
                }
                else // ... otherwise create any missing directories and copy the folder over
                {
                    Directory.CreateDirectory(destFile.DirectoryName); // Does nothing on directories that already exist
                    originalFile.CopyTo(destFile.FullName, false); // Copy but don't over-write  
                }

            });
        }

        // --------------------------------------------------------------
        // DataSets function
        static void BigDataSetFiles()
        {
            string path = @"G:\DataSet\BigDataSet\TrainOrder";
            List<Tuple<int, string>> InfoList = new List<Tuple<int, string>>();
            const int MinCountOfImg = 570;

            foreach (string dirFile in Directory.GetDirectories(path))
            {
                InfoList.Add(new Tuple<int, string>(CountFilesInFolder(dirFile, "jpg"), dirFile));

            }

            foreach (var item in InfoList)
            {
                if (item.Item1 > MinCountOfImg)
                {

                    Copy(item.Item2, @"H:\Magshimim_Project\DataSet" + "\\" + item.Item2.Split('\\')[(item.Item2.Split('\\').Length) - 1]);
                }
            }
            //InfoList.Sort((x, y) => x.Item1.CompareTo(y.Item1)); // sort the list 
        }
        static List<Tuple<int, string>> CountGarden()
        {
            string path = @"G:\DataSet\Garden";
            List<Tuple<int, string>> InfoList = new List<Tuple<int, string>>();

            foreach (string dirFile in Directory.GetDirectories(path))
            {
                InfoList.Add(new Tuple<int, string>(CountFilesInFolder(dirFile, "jpg"), dirFile));

            }
            InfoList.Sort((x, y) => x.Item1.CompareTo(y.Item1)); // sort the list
            return InfoList;
        }

        static void csvStuff()
        {
            List<string> list = new List<string>();

            using (var reader = new StreamReader(@"G:\DataSet\More_DataSets\gbif-observations-dwca\observations.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var a = reader.ReadLine();
                    Console.ReadKey();

                }
            }
        }

        static void Main(string[] args)
        {
            string path = @"H:\Magshimim_Project\DataSet";
            List<Tuple<int, string>> InfoList = new List<Tuple<int, string>>();

            foreach (string dirFile in Directory.GetDirectories(path))
            {
                InfoList.Add(new Tuple<int, string>(CountFilesInFolder(dirFile, "jpg"), dirFile));

            }
            InfoList.Sort((x, y) => x.Item1.CompareTo(y.Item1)); // sort the list
            Console.ReadKey();
        }
    }
}
