﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DownloadFromGarden_Console
{
    class Program
    {
        /// <summary>
        /// Download and load from files, Proxy servers IP
        /// </summary>
        /// <returns> Proxy List</returns>
        static List<string> GetProxyList()
        {
            int i = 0;
            string line;
            string htmlCode;
            string contents;
            string ip = "89.236.17.108";
            string port = "3128";
            List<string> ProxyList = new List<string>
            {
                /*
                "92.53.73.138:8118",
                "89.236.17.108:3128",
                "185.93.3.123:8080",
                "66.82.123.234:8080",
                "59.106.215.9:3128",
                "138.197.131.109:3128",
                "80.211.201.9:8888",
                "128.199.199.41:3128",
                "180.244.245.80:80",
                "88.99.149.188:31288",
                "124.121.173.245:8888"
                */

            };

            // Download And Add To ProxyList From Spys.me
            using (var wc = new System.Net.WebClient())
                contents = wc.DownloadString("http://spys.me/proxy.txt");
            File.WriteAllText(@"G:\DataSet\P.txt", contents);
            Console.WriteLine("Spys.me:\n");
            // Read the file and display it line by line.  
            System.IO.StreamReader file = new System.IO.StreamReader(@"G:\DataSet\P.txt");
            var lineCount = File.ReadLines(@"G:\DataSet\P.txt").Count();

            while ((line = file.ReadLine()) != null)
            {
                if (i > 3 && i < lineCount - 3)
                {
                    string[] tokens = line.Split(' ');
                    if (tokens[1].Contains('S'))
                    {
                        ProxyList.Add(tokens[0]);
                        System.Console.WriteLine(line);

                    }
                }
                i++;
            }
            file.Close();
            //------------------------------------------------------
            // Load ssl.txt > ProxyList
            Console.WriteLine("ssl.txt:\n");
            var lines = File.ReadAllLines(@"G:\DataSet\ssl.txt");

            foreach (var l in lines)
            {
                ProxyList.Add(l);
                System.Console.WriteLine(line);
            }
            //------------------------------------------------------------
            // Download ProxyList From https://www.sslproxies.org/ 

            using (WebClient client = new WebClient())
            {
                htmlCode = client.DownloadString("https://www.sslproxies.org/");
            }

            HtmlAgilityPack.HtmlDocument code = new HtmlAgilityPack.HtmlDocument();

            code.LoadHtml(htmlCode); // Convert The HTML String To Object

            for (i = 1; i < 20; i++)
            {
                try
                {

                    ip = code.DocumentNode.SelectNodes(String.Format("//table[@id='proxylisttable']//tbody[1]//tr[{0}]//td[1]", i))[0].InnerText;
                    port = code.DocumentNode.SelectNodes(String.Format("//table[@id='proxylisttable']//tbody[1]//tr[{0}]//td[2]", i))[0].InnerText;
                    if (ip != null && port != null)
                    {
                        ProxyList.Add(String.Format("{0}:{1}", ip, port));
                        i++;

                    }
                    else
                    {
                        break;
                    }

                }
                catch (Exception e)
                {

                    Console.WriteLine(e.ToString());
                }
            }


            return ProxyList;

        }
        static void Main(string[] args)
        {
            List<string> ProxyList = GetProxyList();
            Garden g = new Garden(ProxyList, @"G:\DataSet\Garden\");
            List<Task> TaskList = new List<Task>();
            Console.WriteLine("Curr_Index:");
            int curr_index = int.Parse(Console.ReadLine());// 7900;
            Console.WriteLine("Number Of loops: (Loop = 100)");
            int l = int.Parse(Console.ReadLine());// 100;
            for (int i = 0; i < l; i++)
            {
                TaskList.Clear();
                Random rnd = new Random();
                Thread.Sleep(rnd.Next(100, 10000));
                for (int j = 0; j < 100; j++)
                {
                    TaskList.Add(g.DownloadAsync(curr_index + j));
                }
                Task.WaitAll(TaskList.ToArray());
                curr_index += 100;
                Console.WriteLine("Waiting....\nDone!\nCurr_Index: " + curr_index.ToString());

            }



            // task.ContinueWith( t => { }).Wait();
            Console.ReadKey();
        }
    }
}
