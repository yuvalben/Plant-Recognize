﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Threading;

namespace DownloadFromGarden_Console
{
    class Garden
    {
        string BaseUrl = "https://garden.org";
        List<string> ProxyList;
        string SavePath; // @"G:\DataSet\Garden\"

        public Garden(List<string> PL, string Path)
        {
            this.ProxyList = PL;
            this.SavePath = Path;
        }

        /// <summary>
        /// Generate String
        /// </summary>
        /// <returns> Random String (len: 8 )</returns>
        public string Get8CharacterRandomString()
        {
            string path = Path.GetRandomFileName();
            path = path.Replace(".", ""); // Remove period.
            return path.Substring(0, 8);  // Return 8 character string
        }

        /// <summary>
        /// return HttpClientHandler with random Proxy
        /// </summary>
        /// <returns> HttpClientHandler With Proxy </returns>
        public HttpClientHandler GetHttpClientHandlerWithProxy()
        {
            Random rnd = new Random();
            string ProxyIP = ProxyList[rnd.Next(ProxyList.Count)];
            HttpClientHandler c = new HttpClientHandler
            {
                Proxy = new WebProxy(ProxyIP, true)
            };
            return c;
        }

        private void DownloadAndSaveImage(HttpClientHandler clientHandler, string url, string PathToSaveTheImg)
        {
            System.IO.Directory.CreateDirectory(PathToSaveTheImg);
            var client = new HttpClient(clientHandler);
            //var stream = client.GetStreamAsync(url).Result;
            using (var yourImage = Image.FromStream(client.GetStreamAsync(url).Result))
            {
                if (url.Substring(url.Length - 4) == ".jpg")
                {

                    //yourImage.Save(@PathToSaveTheImg + "\\" + Get8CharacterRandomString() + ".jpg", ImageFormat.Jpeg);
                    SaveJpeg(@PathToSaveTheImg + "\\" + Get8CharacterRandomString() + ".jpg", yourImage, 100);
                }
                else if (url.Substring(url.Length - 4) == ".png")
                {
                    yourImage.Save(PathToSaveTheImg + "\\" + Get8CharacterRandomString() + ".png", ImageFormat.Png);

                }
                else if (url.Substring(url.Length - 4) == ".Bmp")
                {
                    yourImage.Save(PathToSaveTheImg + "\\" + Get8CharacterRandomString() + ".Bmp", ImageFormat.Bmp);

                }
                else
                {
                    Console.WriteLine("WTF?!");
                    Console.WriteLine(url);
                }

            }
        }

        static async Task<string> DownloadPage(string url, HttpClientHandler clientHandler)
        {

            using (var client = new HttpClient(clientHandler))
            {

                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");


                using (var r = await client.GetAsync(new Uri(url)))
                {
                    string result = await r.Content.ReadAsStringAsync();
                    return result;
                }
            }
        }

        private static void SaveJpeg(string path, Image img, int quality)
        {
            EncoderParameter qualityParam
            = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);

            ImageCodecInfo jpgEncoder = ImageCodecInfo.GetImageEncoders().First(c => c.FormatID == ImageFormat.Jpeg.Guid);

            EncoderParameters encoderParams
            = new EncoderParameters(1);

            encoderParams.Param[0] = qualityParam;

            System.IO.MemoryStream mss = new System.IO.MemoryStream();

            System.IO.FileStream fs
            = new System.IO.FileStream(path, System.IO.FileMode.Create
            , System.IO.FileAccess.ReadWrite);

            img.Save(mss, jpgEncoder, encoderParams);
            byte[] matriz = mss.ToArray();
            fs.Write(matriz, 0, matriz.Length);

            mss.Close();
            fs.Close();
        }



        public async Task DownloadAsync(int index)
        {
            string result;
            int LoopCount = 5;

            try
            {
                //string IP= await DownloadPage("https://api.ipify.org/",clientHandler); // Debug Only!
                try
                {
                    result = await DownloadPage((BaseUrl + "/plants/photo/" + index.ToString() + "/"), GetHttpClientHandlerWithProxy()); // Download The Html        

                }
                catch (Exception)
                {
                    Random r = new Random();

                    int m = r.Next(1000, 8000);
                    Console.WriteLine("Index: " + index.ToString() + "Waiting: " + m.ToString() + " Milisec");
                    Thread.Sleep(m);
                    result = await DownloadPage((BaseUrl + "/plants/photo/" + index.ToString() + "/"), GetHttpClientHandlerWithProxy()); // Download The Html        

                }

                if (!result.Contains("was not found on this server") || !result.Contains("Excessive requests from your IP has resulted in an automatic block being added to our system."))
                {
                    HtmlAgilityPack.HtmlDocument code = new HtmlAgilityPack.HtmlDocument();

                    code.LoadHtml(result); // Convert The HTML String To Object

                    // Find The Img Src
                    var ImgUrl = code.DocumentNode.SelectNodes("//*[contains(@class,'rwd_photo')]").ToArray()[0].Attributes[0].DeEntitizeValue;

                    // Get Image Title
                    var ImgTitle = result.Split(new[] { "Plant: " }, StringSplitOptions.None)[1].Split('>', '(')[1];
                    Console.WriteLine(ImgTitle.ToString() + ", " + ImgUrl.ToString());
                    Random rnd = new Random();

                    int m = rnd.Next(1000, 8000);
                    Console.WriteLine("Index: "+index.ToString()+"Waiting: " + m.ToString()+" Milisec");
                    Thread.Sleep(m);
                    
                    while (LoopCount > 0)
                    {
                        try
                        {
                            DownloadAndSaveImage(GetHttpClientHandlerWithProxy(), BaseUrl + ImgUrl, this.SavePath + ImgTitle);
                            break;
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("inedx number: " + index.ToString() + " Status: Try Again - " + LoopCount.ToString());
                            DownloadAndSaveImage(GetHttpClientHandlerWithProxy(), BaseUrl + ImgUrl, this.SavePath + ImgTitle);
                            LoopCount--;
                        }
                    }
                    

                }
                else
                {
                    Console.WriteLine("Index: " + index.ToString() + " Without an Image");
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("------------------------------------");
                Console.WriteLine("Exception: " + e.Message.ToString() + "index: " + index.ToString() + "\nMore Info: "+ e.InnerException.InnerException.Message.ToString());
                //Helper.WriteToXmlFile<Exception>(@"G:\DataSet\Garden\EX\"+Get8CharacterRandomString()+".xml", e);
                
                if (e.InnerException.InnerException.Message == "The remote server returned an error: (403) Forbidden.")
                {
                    var ServerBanIP = e.InnerException.InnerException;
                    Console.WriteLine("Index: " + index.ToString() + " The remote server returned an error: (403) Forbidden.");
                }
            }

        }
    }
}

