"""
The code loads a text file that contains folder names and the amount of files in its folders.
Receives as the input the minimum number of files in its folders and prints all the files that
have more files than the input


"""
from itertools import islice

def take(n, iterable):
    return list(islice(iterable, n))

with open("Info1.txt","r") as f:
    content = f.readlines()
content = [x.strip() for x in content]
for line in content:
    Count = str(line).split(' ')[1]
    FN = str(line).split(' ')[0]
    if int(Count) > 600:
        print(FN+"Count: "+str(Count))
    
