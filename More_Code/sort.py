import xml.etree.ElementTree as ET
import json
from shutil import copyfile
import os


DB = {}
arr = os.listdir("G:\\DataSet\\BigDataSet\\train")
ErrorCount = 0
for index in range(1,113204):
	try:
		tree = ET.parse(os.path.join("G:\\DataSet\\BigDataSet\\train",(str(index)+'.xml')))
		root = tree.getroot()
		if str(root[6].text) in DB: # Check if The Folwer in The DB
			count = DB.get(str(root[6].text))
			DB[str(root[6].text)] = (count+1) # Add To The DB The Flower Count
			#copyfile((os.path.join("G:\DataSet\BigDataSet\train",(str(index)+".jpg"))) , (os.path.join(os.path.join("G:\DataSet\BigDataSet\TrainOrder",root[6].text),(str(index)+".jpg")) ))
			copyfile((str(index)+".jpg") , (os.path.join(os.path.join("G:\DataSet\BigDataSet\TrainOrder",root[6].text),(str(index)+".jpg"))))

		else:
			DB.update({str(root[6].text) : 1})
			dir = os.path.join("G:\DataSet\BigDataSet\TrainOrder",str(root[6].text)) #Build The Flower Folder Path
			if not os.path.exists(dir):
				os.makedirs(dir) #Create the Dir
				#copyfile(os.path.join("G:\DataSet\BigDataSet\train",(str(index)+".jpg")),os.path.join(dir,(str(index)+".jpg")))
				copyfile(((str(index)+".jpg")),os.path.join(dir,(str(index)+".jpg")))

		print(index/113204*100," percent complete         \r",end='')		 
	except IOError as e:
		print(str(e)) #Debug Only
		Error = (str(e)).split(" ") 
		if (Error[2]) == "No":
			if Error[-1] in arr:
				ErrorCount+1
		#input("Next?") #Debug Only
		pass

print("-----------------------------------------")		
print(len(DB))
print("Error Count: "+ ErrorCount)
with open(str('data -'+str(len(DB))+ '.json'), 'w') as outfile:
    json.dump(DB, outfile)
print(json.dumps(DB))
input("Pause")
