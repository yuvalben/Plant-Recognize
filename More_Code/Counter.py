"""
The code counts all the files in each folder
and save the output to a file (txt file)

For Example:

 input:
	Folder Name: MainDir
	
 output:
	- MainDir
		- Rose 3875 
		- Sunflower 6547
		- Tulip 986
		- More 304
 
"""
import os
import sys
sys.stdout = open("Info1.txt","w+")

path = "H:\\Magshimim_Project\\DataSet_Beta\\train_val2018\\train_val2018\\Insecta"
folders = ([name for name in os.listdir(path)])
targets = []
for folder in folders:
    contents = os.listdir(os.path.join(path,folder))
    for i in contents:
        if i.endswith('.shp'):
            targets.append(i)
    print(folder, len(contents))

os.close(0)
