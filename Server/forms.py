import django.forms
from django.contrib.auth.models import User
from .models import Flowers


class ImageUpload(django.forms.Form):
    file = django.forms.ImageField()


class UserForm(django.forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'password', 'first_name', 'last_name', 'email']


class FlowerForm(django.forms.ModelForm):
    class Meta:
        model = Flowers
        fields = ['id', 'name', 'description', 'hebname']
