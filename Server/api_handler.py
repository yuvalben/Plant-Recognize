import Server.loginHandler
from django.shortcuts import render, HttpResponse, Http404
from django.contrib import auth
from rest_framework.parsers import JSONParser
#from rest_framework.response import Response
import json
import rest_framework.authentication


def Response(data):
    return HttpResponse(json.dumps(data))

class Errors:
    NoError = {'error': None}
    UsernamePasswordDontMatch = {'error': {'id': '2', 'text': 'Username or password do not match'}}
    InvalidOperation = {'error': {'id': '3', 'text': 'Invalid operation'}}
    UserNotLoggedIn = {'error': {'id': '1', 'text': 'User is not logged in'}}

    @staticmethod
    def genrate(id, text):
        return {'error': {'id': id, 'text': text}}

from rest_framework.decorators import api_view
@api_view(['GET'])
def api_login(request):
    if request.method == 'GET':
        data = json.loads(request.GET['data'])
        if Server.loginHandler.isLoggedIn(request):
            auth.logout(request)
        user_logged = auth.authenticate(request, username=data['username'], password=data['password'])
        if user_logged is not None:
            auth.login(request, user_logged)
            return Response(data=Errors.NoError)
        return Response(data=Errors.UsernamePasswordDontMatch)
    return Response(data=Errors.InvalidOperation)


def api_upload(request):
    if not Server.loginHandler.isLoggedIn(request):
        return Response(data=Errors.UserNotLoggedIn)

