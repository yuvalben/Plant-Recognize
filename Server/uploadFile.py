from django.http import HttpResponseRedirect, HttpResponse, HttpResponseNotFound
from django.shortcuts import render, loader
from .loginHandler import isLoggedIn
from Server.models import Flowers, Search
# from .forms import UploadFileForm

from .FlowerRecognize import Classifier2
import tensorflow as tf

g = tf.get_default_graph()
# Imaginary function to handle an uploaded file.
# from somewhere import handle_uploaded_file

classifier = Classifier2.Classifier(243, 243, 50, 16, '')


def mainwin(request):
    if not isLoggedIn(request):
        return render(request, 'landing.html')
    return render(request, 'user_homepage.html')


import django.contrib.auth

import Server.forms


# import django.http as h
# h.HttpRequest().
def upload_file(request):
    if not isLoggedIn(request):
        return HttpResponseRedirect('/login')
    # print('gotcha')
    if request.method == 'POST':
        print('post')

        # form = UploadFileForm(request.POST, request.FILES)
        # print(form)
        try:
            res_id, val, id = handle_uploaded_file(request.FILES['file'])
        except:
            return HttpResponse('No file...')
        print(res_id)
        flower = Flowers.objects.filter(id=res_id)
        if len(flower):
            flower = flower.first()
            flower.searches += 1
            flower.save()
            return render(request, 'result page.html',
                          context={'flower_name': flower.name, 'hebrew_name': flower.hebname,
                                   'data': flower.description,
                                   'flower_image': '/static/Flowers/' + str(res_id) + '.jpg',
                                   'uploaded_image': request.FILES['file'], 'precents': "%.1f" % val,
                                   'last_searched': flower.searches, 'id': str(id)})

        return HttpResponse('bad flower')
    return render(request, 'upload.html')


from datetime import datetime


def handle_uploaded_file(image_file):
    global g
    global classifier
    # "good.jpg".endswith('.jpg')
    filename = 'Images/' + str(datetime.now().strftime('%d-%m-%Y_%H-%m-%S'))
    with open(filename, 'wb+') as f:
        # if image_file.name.lower().endswith('.jpg'):
        file_stream = b''
        for chunk in image_file.chunks():
            file_stream += chunk
        f.write(file_stream)
        f.close()
    with g.as_default():
        id, val = classifier.Predict(image_file)
        s = Search(image=filename, result=id)
        s.save()
        return id, val, s.id


def report(request, id):
    search = Search.objects.filter(id=id)
    if not len(search):
        return HttpResponseNotFound('Not Found')
    search[0].report()
    return HttpResponse('<h1>Thanks!</h1>')
