import django.db.models as models


class Flowers(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.TextField()
    hebname = models.TextField()
    description = models.TextField()
    searches = models.IntegerField(default=0)


class Search(models.Model):
    image = models.FilePathField()
    result = models.SmallIntegerField()

    def report(self):
        new_name = self.image
        with open(new_name, 'rb') as oldf:
            oldi = oldf.read()
            oldf.close()

        new_name = new_name.replace('Images/', 'Images/Report/') + '_RESULT_' + str(self.result)
        with open(new_name, 'wb') as f:
            f.write(oldi)
