from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
import PIL
from json import load
import os.path


class Classifier:
    __Image_Width, __Image_Height = 150, 150
    __Train_Data_Dir = 'H:\Magshimim_Project\DataSet'
    __Weights_Dir = 'Flowers.h5'
    __Class_Mapping = {}
    __ClassCount = 32
    __Epochs = 50
    __Batch_size = 16

    def __init__(self, img_w, img_h, epochs, batch_size):
        self.__Image_Height = img_h
        self.__Image_Width = img_w
        self.__Epochs = epochs
        self.__Batch_size = batch_size
        self.__Model = self.load_trained_model(self.__Weights_Dir)
        with open('Server/FlowerRecognize/ClassMapping.txt', 'r') as f:
            self.__Class_Mapping = load(f)
            f.close()

    def Train(self, TrainDataDir):
        # this is the augmentation configuration we will use for training
        Train_DataGen = ImageDataGenerator(
            rescale=1. / 255,
            shear_range=0.2,
            zoom_range=0.2,
            horizontal_flip=True)

        Train_Generator = Train_DataGen.flow_from_directory(
            TrainDataDir,
            target_size=(self.__Image_Width, self.__Image_Height),
            batch_size=self.__Batch_size,
            class_mode='categorical')

        self.__Class_Mapping = Train_Generator.class_indices

        self.__Model.fit_generator(
            Train_Generator,
            steps_per_epoch=2000 // self.__Batch_size,
            epochs=self.__Epochs,
        )

        self.__Model.save_weights(self.__Weights_Dir)

        with open("ClassMapping.txt") as f:
            f.write(self.__Class_Mapping.__repr__())

    def Predict(self, imgsrc):
        Image = load_img(imgsrc)
        ImageArray = img_to_array(Image)
        ImageArray.resize((150, 150, 3))  # this is a Numpy array with shape (3, 150, 150)
        ImageArray = ImageArray.reshape((1,) + ImageArray.shape)  # this is a Numpy array with shape (1, 3, 150, 150)

        # x = self.__Model.predict(ImageArray, batch_size=None, verbose=0).tolist()[0]
        x=[0,0.25,0.75]
        print(x)
        max_value = max(x)
        index = x.index(max_value)
        max_value *= 100
        # print(x)
        return index, max_value

    def BuildModel(self):
        Model = Sequential()
        Model.add(Conv2D(32, (3, 3), input_shape=(150, 150, 3)))
        Model.add((Activation('relu')))
        Model.add(MaxPooling2D(pool_size=(2, 2)))

        Model.add(Conv2D(32, (3, 3)))
        Model.add(Activation('relu'))
        Model.add(MaxPooling2D(pool_size=(2, 2)))

        Model.add(Conv2D(64, (3, 3)))
        Model.add(Activation('relu'))
        Model.add(MaxPooling2D(pool_size=(2, 2)))
        # the model so far outputs 3D feature maps (height, width, features)

        Model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
        Model.add(Dense(64))
        Model.add(Activation('relu'))
        Model.add(Dropout(0.5))
        Model.add(Dense(self.__ClassCount))
        Model.add(Activation('softmax'))

        Model.compile(loss='categorical_crossentropy',
                      optimizer='adam')
        return Model

    def load_trained_model(self, weights_path):
        model = self.BuildModel()
        if os.path.exists(weights_path):
            model.load_weights(weights_path)
        return model

    def ImageToBatch(self, src_path, dest_folder_path, multi):
        """
                       width_shift and height_shift are ranges (as a fraction of total width or height)
                       within which to randomly translate pictures vertically or horizontally.

                       rescale is a value by which we will multiply the data before any other processing.
                       Our original images consist in RGB coefficients in the 0-255,
                       but such values would be too high for our models to process (given a typical learning rate),
                       so we target values between 0 and 1 instead by scaling with a 1/255. factor.

                       fill_mode is the strategy used for filling in newly created pixels,
                       which can appear after a rotation or a width/height shift.

                       Fill_Mode_Options = {"constant", "nearest", "reflect", "wrap"}

                       """
        DataGen = ImageDataGenerator(

            width_shift_range=0.2,
            height_shift_range=0.2,
            rescale=1. / 255,
            fill_mode='reflect'
        )

        Image = load_img(src_path)
        ImageArray = img_to_array(Image)  # this is a Numpy array with shape (3, 150, 150)
        ImageArray = ImageArray.reshape((1,) + ImageArray.shape)  # this is a Numpy array with shape (1, 3, 150, 150)

        # the .flow() command below generates batches of randomly transformed images
        # and saves the results to the `preview/` directory
        # for i in range(multi):
        i = 0
        for batch in DataGen.flow(ImageArray, batch_size=1, save_to_dir=dest_folder_path, save_prefix='Flower',
                                  save_format='jpeg'):
            i + 1
            if i > 5:
                break;

    def TrainMainDataset(self):
        self.Train(self.__Train_Data_Dir)
