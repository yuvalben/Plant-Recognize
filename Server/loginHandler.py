from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
import django.contrib.auth as auth
from Server.models import Flowers
from django.contrib.auth.models import User
from .forms import FlowerForm


def reset(r):
    User.objects.all().delete()
    user = User.objects.create_user(username='good', password='good', email='huckcbm@gmail.com',
                                    first_name='yuval',
                                    last_name='benz')
    user.save()
    Flowers.objects.all().delete()
	


def addF(req):
    if req.method == 'POST':
        f = FlowerForm(req.POST)
        if f.is_valid():
            f.save()
        else:
            return HttpResponse(str(f.errors))
        return redirect('/new')
    return render(req, 'register.html', {'form': FlowerForm})


def isLoggedIn(request):
    user = request.user
    return user.is_active and user.is_authenticated


def userchack(req):
    if req.method == "GET":
        u = req.GET['u']
        return HttpResponse('1' * len(User.objects.filter(username=u)))


def trylogin(request, username, password):
    u = auth.authenticate(request, username=username, password=password)
    if u:
        auth.login(request, u)
    return u


# django.contrib.auth.models import User


def switchPass(req):
    old = req.GET['old']
    passw = req.GET['new']
    u = trylogin(req, username='good', password=old)
    if u is not None:
        passw = User.objects.make_random_password()
        u.set_password(passw)
        u.save()
        auth.logout(req)
        return HttpResponse(passw)
    return HttpResponse('BAD')


def login(request):
    """

    :type request:
    """
    if isLoggedIn(request):
        return HttpResponseRedirect('/logout')
    if request.method == 'POST':
        user_logged = trylogin(request, username=request.POST['username'], password=request.POST['password'])
        if user_logged is not None:
            return HttpResponseRedirect('/')
        else:
            return render(request, 'home.html', context={'string': '<b>Username or password is invalid</b><br>'})
    if request.method == 'GET':
        return redirect('/#login')


def logout(request):
    """
    :type request:django.http.HttpRequest
    """
    auth.logout(request)
    # request.user.logout()
    return HttpResponseRedirect('/')


from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from .forms import UserForm


def register(request):
    """

    :type request: django.http.HttpRequest
    """

    if (request.method == "POST"):
        email = request.POST['email']
        username = request.POST['name']
        passw = request.POST['password']
        if User.objects.filter(username=username).exists():
            username_unique_error = True

        if User.objects.filter(email=email).exists():
            email_unique_error = True

        else:
            create_new_user = User.objects.create_user(username, email, passw)
            create_new_user.save()
            user = auth.authenticate(username=username, password=passw)
            auth.login(request, user)
            if create_new_user is not None:
                if create_new_user.is_active:
                    return redirect('/')
                else:
                    print("The password is valid, but the account has been disabled!")
    return redirect('/')
