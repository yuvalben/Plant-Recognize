"""Server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import uploadFile, loginHandler
import Server.api_handler
urlpatterns = [
    path('username',loginHandler.userchack),
    path('upload', uploadFile.upload_file),
    path('', uploadFile.mainwin),
    path('logout', loginHandler.logout),
    path('login/', loginHandler.login),
    path('new', loginHandler.addF),
    path('register/', loginHandler.register),
    path('reset',loginHandler.reset),
    path('report/<int:id>',uploadFile.report)
]
